import React, { useState, useRef } from "react";
import "./App.css";
import TodoListToday from "./components/TodoListToday";
import TodoListTomorrow from "./components/TodoListTomorrow";
import { v4 as uuidv4 } from "uuid";
import trash from "./images/trash.svg" 
import left from "./images/arrow-left.svg" 
import right from "./images/arrow-right.svg" 

function App() {
  const [todos, setTodos] = useState([]);
  const [day, setDay] = useState("today");
  const todoNameRef = useRef();

  function toggleTodo(id) {
    const newTodos = [...todos];
    const todo = newTodos.find((todo) => todo.id === id);
    todo.complete = !todo.complete;
    setTodos(newTodos);
  }

  function handleAddTodo(e) {
    e.preventDefault();
    const name = todoNameRef.current.value;
    setTodos((prevTodos) => {
      return [
        ...prevTodos,
        { id: uuidv4(), name: name, complete: false, day: day },
      ];
    });
    todoNameRef.current.value = null;
    setDay("today");
  }

  function handleClearTodos() {
    const newTodos = todos.filter((todo) => !todo.complete);
    setTodos(newTodos);
  }

  const handleChange = (event) => {
    setDay(event.target.value);
    console.log(day);
  };

  function handleTomorrow() {
    const checked = todos.filter((todo) => todo.complete);
    checked.forEach((todo) => {
      todo.day = "tomorrow";
      todo.complete = false
    });
    setTodos((prevTodos) => {
      return [...prevTodos, checked];
    });
  }

  function handleToday() {
    const checked = todos.filter((todo) => todo.complete);
    checked.forEach((todo) => {
      todo.day = "today";      
    });
    todos.forEach((todo) =>{
      todo.complete = false;
    })
    setTodos((prevTodos) => {
      return [...prevTodos, checked];
    });
  }

  return (
    <>
      <div className="container">
        <div className="form">
          <form>
            <div>
              <h2>Add new task</h2>
            </div>
            <input className="form-elements" ref={todoNameRef} type="text" />
            <input
            className="form-elements"
              type="radio"
              value="today"
              checked={day === "today"}
              onChange={handleChange}
            />
            today
            <input
            className="form-elements"
              type="radio"
              value="tomorrow"
              checked={day === "tomorrow"}
              onChange={handleChange}
            />
            tomorrow
            <button className="form-elements" onClick={handleAddTodo}>add todo</button>
          </form>
        </div>
        <div className="row">
          <div className="todos">
            <h2>Tasks for today</h2>
            <TodoListToday todos={todos} toggleTodo={toggleTodo} day={day} />
          </div>
          <div className="actions">
            <button className="button" onClick={handleClearTodos}>
              <img className="icon" src={trash} />
            </button>
            <button className="button" onClick={handleTomorrow}>
              <img className="icon" src={right} />
            </button>
            <button className="button" onClick={handleToday}>
              <img className="icon" src={left} />              
            </button>
          </div>
          <div className="todos">
            <h2>Tasks for tomorrow</h2>
            <TodoListTomorrow todos={todos} toggleTodo={toggleTodo} day={day} />
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
